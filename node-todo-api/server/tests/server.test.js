const expect = require('expect')
const request = require('supertest')
const {ObjectID} = require('mongodb')

const {app} = require('./../server.js')
const {Todo} = require('./../models/todo.js')
const {User} = require('./../models/user.js')
const {todos, users, populateTodos, populateUsers} = require('./seed/seed')

beforeEach(populateUsers)
beforeEach(populateTodos)

describe('/todos',()=>{

	describe('POST',()=>{

		it('should create a new todo',(done)=>{
			let user = users[0]
			let text = 'Test this route'
			request(app)
				.post('/todos')
				.set('x-auth',user.tokens[0].token)
				.send({text})
				.expect(200)
				.expect((res)=>{
					expect(res.body.text).toBe(text)
				})
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.find({text}).then((doc)=>{

						expect(doc.length).toBe(1)
						expect(doc[0].text).toBe(text)
						done()
					}).catch((e)=>done(e))
				})
		})

		it('should not create a new todo with invalid data',(done)=>{
			let user = users[0]
			request(app)
				.post('/todos')
				.set('x-auth',user.tokens[0].token)
				.send({})
				.expect(400)
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.find().then((doc)=>{
						expect(doc.length).toBe(todos.length)
						done()
					}).catch((e)=>done(e))
				})
		})

	})

	describe('GET',()=>{

		it('should list all todos',(done)=>{
			let user = users[0]
			request(app)
				.get('/todos')
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(200)
				.expect((res)=>{
					expect(res.body.todos.length).toBe(1)
				})
				.end(done)

		})

		it('should return 200 with present id',(done)=>{
			let user = users[0]
			request(app)
				.get(`/todos/${todos[0]._id.toHexString()}`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(200)
				.expect((res)=>{
					expect(res.body.todo.text).toEqual(todos[0].text)
				})
				.end(done)
		})

		it('should return 404 with absent id',(done)=>{
			let user = users[0]
			request(app)
				.get(`/todos/${new ObjectID().toHexString()}`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(404)
				.expect((res)=>{
					expect(res.body).toEqual({})
				})
				.end(done)
		})

		it('shoudl return 404 with invalid id',(done)=>{
			let user = users[0]
			request(app)
				.get(`/todos/123`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(404)
				.expect((res)=>{
					expect(res.body).toEqual({})
				})
				.end(done)

		})

		it('should not return doc created by another user',(done)=>{
			let user = users[1]
			request(app)
				.get(`/todos/${todos[0]._id.toHexString()}`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(404)
				.end(done)
		})

	})

	describe('DELETE',()=>{

		it('should delete todo with given id',(done)=>{
			
			let todo = todos[0]
			let user = users[0]
			let id = todo._id.toHexString()

			request(app)
				.delete(`/todos/${id}`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(200)
				.expect((res)=>{
					expect(res.body.todo.text).toEqual(todo.text)
				})
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.findById(id).then((todo)=>{
						expect(todo).toBeFalsy()
						done()
					}).catch((e)=>done(e))
				})

		})

		it('should not delete todo belonging to different user',(done)=>{
			
			let todo = todos[0]
			let user = users[1]
			let id = todo._id.toHexString()

			request(app)
				.delete(`/todos/${id}`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(404)
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.findById(id).then((todo)=>{
						expect(todo).toBeTruthy()
						done()
					}).catch((e)=>done(e))
				})

		})

		it('should return 404 with absent id',(done)=>{
			let user = users[0]
			request(app)
				.delete(`/todos/${new ObjectID().toHexString()}`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(404)
				.expect((res)=>{
					expect(res.body).toEqual({})
				})
				.end(done)
		})

		it('shoudl return 404 with invalid id',(done)=>{
			let user = users[0]
			request(app)
				.delete(`/todos/123`)
				.set('x-auth',user.tokens[0].token)
				.send()
				.expect(404)
				.expect((res)=>{
					expect(res.body).toEqual({})
				})
				.end(done)

		})

	})


	describe('PATCH',()=>{
		
		it('Should update todo',(done)=>{
			let user = users[0]
			let oldTodo = todos[0]
			let oldTodoId = oldTodo._id.toHexString()
			let updatedTodo = {
				text: 'Sample Test #1 (Updated)',
				completed: true
			}

			request(app)
				.patch(`/todos/${oldTodoId}`)
				.set('x-auth',user.tokens[0].token)
				.send(updatedTodo)
				.expect(200)
				.expect((res)=>{
					expect(res.body.todo.completed).toBe(updatedTodo.completed)
					expect(res.body.todo.text).toBe(updatedTodo.text)
					expect(typeof res.body.todo.completedAt).toBe('number')
				})
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.findById(oldTodoId).then((doc)=>{
						expect(doc.text).toBe(updatedTodo.text)
						expect(doc.completed).toBe(updatedTodo.completed)
						done()
					}).catch((e)=>done(e))
				})
		})

		it('Should not update todo created by other user',(done)=>{
			let user = users[1]
			let oldTodo = todos[0]
			let oldTodoId = oldTodo._id.toHexString()
			let updatedTodo = {
				text: 'Sample Test #1 (Updated)',
				completed: true
			}

			request(app)
				.patch(`/todos/${oldTodoId}`)
				.set('x-auth',user.tokens[0].token)
				.send(updatedTodo)
				.expect(404)
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.findById(oldTodoId).then((doc)=>{
						expect(doc.text).not.toBe(updatedTodo.text)
						expect(doc.completed).not.toBe(updatedTodo.completed)
						done()
					}).catch((e)=>done(e))
				})
		})

		it('Removes completedAt when completed is false',(done)=>{
			let user = users[1]
			let oldTodo = todos[1]
			let oldTodoId = oldTodo._id.toHexString()
			let updatedTodo = {
				text: `${oldTodo.text} (Updated)`,
				completed: false
			}

			request(app)
				.patch(`/todos/${oldTodoId}`)
				.set('x-auth',user.tokens[0].token)
				.send(updatedTodo)
				.expect(200)
				.expect((res)=>{
					expect(res.body.todo.completed).toBe(updatedTodo.completed)
					expect(res.body.todo.text).toBe(updatedTodo.text)
					expect(res.body.todo.completedAt).toBeFalsy()
				})
				.end((err,res)=>{
					if(err){
						return done(err)
					}

					Todo.findById(oldTodoId).then((doc)=>{
						expect(doc.text).toBe(updatedTodo.text)
						expect(doc.completed).toBe(updatedTodo.completed)
						done()
					}).catch((e)=>done(e))
				})
		})

	})
})

describe('GET /users/me',()=>{

	it('should return user if authenticated',(done)=>{

		request(app)
			.get('/users/me')
			.set('X-AUTH',users[0].tokens[0].token)
			.expect(200)
			.expect((res)=>{
				expect(res.body._id).toBe(users[0]._id.toHexString())
				expect(res.body.email).toBe(users[0].email)
			})
			.end(done)
	})

	it('should return 401 if user is not authenticated',(done)=>{
		
		request(app)
			.get('/users/me')
			.expect(401)
			.expect((res)=>{
				expect(res.body).toEqual({})
			})
			.end(done)
	})

})

describe('POST /users/',()=>{

	it('should create a user',(done)=>{
		
		const email = 'unique@gmail.com'
		const password = 'password123'

		request(app)
			.post('/users')
			.send({email,password})
			.expect(201)
			.expect((res)=>{
				expect(res.headers['x-auth']).toBeTruthy()
				expect(res.body._id).toBeTruthy()
				expect(res.body.email).toBe(email)
			})
			.end((err)=>{
				if (err) {
					return done(err)
				}

				User.findOne({email: email}).then((user)=>{
					expect(user).toBeTruthy()
					expect(user.password).not.toBe(password)
					expect(user.email).toBe(email)
					done()
				})
			})
	})

	it('should return validation errors if request invalid',(done)=>{
		
		const email = '@gmailcom'
		const password = 'p'

		request(app)
			.post('/users')
			.send({email,password})
			.expect(400)
			.expect((res)=>{
				expect(res.headers['x-auth']).toBeFalsy()
			})
			.end((err)=>{
				if (err) {
					return done(err)
				}

				User.count({email: email}).then((count)=>{
					expect(count).toBe(0)
					done()
				}).catch((e)=> done(e))
			})
	})

	it('should not create user if email is in use',(done)=>{
		const email = users[0].email
		const password = 'password123'
		request(app)
			.post('/users')
			.send({email,password})
			.expect(400)
			.expect((res)=>{
				expect(res.headers['x-auth']).toBeFalsy()
			})
			.end((err)=>{
				if (err) {
					return done(err)
				}

				User.count({email: email}).then((count)=>{
					expect(count).toBe(1)
					done()
				}).catch((e)=>done(e))
			})
	})

})

describe('POST /users/login',()=>{
	it('should login user and return auth token',(done)=>{
		
		const user = users[1]

		const creds = {
			email: user.email,
			password: user.password
		}

		request(app)
			.post('/users/login')
			.send(creds)
			.expect(200)
			.expect((res)=>{
				expect(res.headers['x-auth']).toBeTruthy()
				expect(res.body._id).toBe(user._id.toHexString())
				expect(res.body.email).toBe(user.email)
			})
			.end((err,res)=>{
				if(err){
					return done(err)
				}

				let token = res.headers['x-auth']
				User.findById(user._id.toHexString()).then((resUser)=>{
					expect(resUser.tokens[1].token).toBe(token)
					done()
				}).catch((e)=>done(e))
			})
	})

	it('should reject invalid login', (done)=>{
		const user = users[1]

		const creds = {
			email: user.email,
			password: `$$$${user.password}$$$`
		}

		request(app)
			.post('/users/login')
			.send(creds)
			.expect(401)
			.expect((res)=>{
				expect(res.headers['x-auth']).toBeFalsy()
			})
			.end(done)
	})
})

describe('DELETE /users/me/token',()=>{

	it('should remove auth token on logout',(done)=>{

		const user = users[0]

		request(app)
			.delete('/users/me/token')
			.set('x-auth',user.tokens[0].token)
			.send()
			.expect(200)
			.end((err)=>{
				if(err){
					return done(err)
				}

				User.findById(user._id.toHexString()).then((res)=>{
					//console.log(`\n\nRETURNED: ${res}\n\n`);
					expect(res.tokens.length).toBe(0)
					done()
				}).catch((e)=>done(e))
			})
	})

})
