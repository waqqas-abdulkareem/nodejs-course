const {ObjectID} = require('mongodb')
const jwt = require('jsonwebtoken')

const {Todo} = require('./../../models/todo.js')
const {User, signature} = require('./../../models/user.js')

const user1ObjectID = new ObjectID()
const user2ObjectID = new ObjectID()
const users = [{
	_id: user1ObjectID,
	email: 'test.one@gmail.com',
	password: 'password123',
	tokens:[{
		access: 'auth',
		token: jwt.sign({_id: user1ObjectID, access: 'auth'},signature).toString()
	}]
},{
	_id: user2ObjectID,
	email: 'test.two@gmail.com',
	password: 'password234',
	tokens:[{
		access: 'auth',
		token: jwt.sign({_id: user2ObjectID, access: 'auth'},signature).toString()
	}]
}]

const todos = [{
	_id: new ObjectID(),
	text: 'Sample Test #1',
	_creator: user1ObjectID
},{
	_id: new ObjectID(),
	text: 'Sample Test #2',
	completed: true,
	completedAt: new Date(),
	_creator: user2ObjectID
}]

const populateTodos = (done) => {
	Todo.remove({}).then(()=>{
		return Todo.insertMany(todos)
	}).then(()=>{
		done()
	})
}

const populateUsers = (done) => {
	User.remove({}).then(()=>{
		let userOne = new User(users[0]).save()
		let userTwo = new User(users[1]).save()
		return Promise.all([userOne,userTwo])
	}).then(()=>done())
}

module.exports = {
	todos,
	users,
	populateTodos,
	populateUsers
}