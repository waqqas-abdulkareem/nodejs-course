const fs = require("fs");

const notesDb = "./notes.txt";

let fetchNotes = () => {
	try{
		var notesJSONString = fs.readFileSync(notesDb);
		return JSON.parse(notesJSONString);
	}catch(e){
		return [];
	}
}

let saveNotes = (notes) => {
	fs.writeFileSync(notesDb, JSON.stringify(notes));
}

let addNote = (title, body) => {
	let notes = fetchNotes();
	
	let existingTitle = notes.some((note) => note.title === title );

	if (existingTitle) {
		return null;
	}

	let note = {title, body};
	notes.push(note);
	saveNotes(notes);
	return note;
}

let getAll = () => {
	return fetchNotes();
}

let getNote = (title) => {
	let notes = fetchNotes()
	let matchingNotes = notes.filter((note) => note.title === title);
	return matchingNotes.length == 0 ? null : matchingNotes[0];
}

let removeNote = (title) => {
	let notes = fetchNotes();
	let filteredNotes = notes.filter((note) => note.title !== title );
	saveNotes(filteredNotes);

	return filteredNotes.length !== notes.length;
}

module.exports = {
	addNote,
	getAll,
	getNote,
	removeNote
}