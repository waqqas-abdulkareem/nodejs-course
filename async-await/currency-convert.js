//http://data.fixer.io/api/latest?access_key=216e2d854302ff83726a9460b1b3b9e5&format=1
//https://restcountries.eu/rest/v2/currency/kwd
const axios = require('axios')

const FIXER_ACCESS_KEY = '216e2d854302ff83726a9460b1b3b9e5'

// const getExchangeRate = (from,to) => {
// 	return axios.get(`http://data.fixer.io/api/latest?access_key=${FIXER_ACCESS_KEY}`)
// 		.then((response)=>{
// 			const fromRate = response.data.rates[from]
// 			const toRate = response.data.rates[to]
// 			const rate = (1/fromRate) * toRate
// 			return rate
// 		})
// }

const getExchangeRate = async (from,to) => {
	try{
		const response = await axios.get(`http://data.fixer.io/api/latest?access_key=${FIXER_ACCESS_KEY}`)
		const fromRate = response.data.rates[from]
		const toRate = response.data.rates[to]

		if (isNan(fromRate) || isNan(toRate)) {
			throw new Error();
		}

		return (1/fromRate) * toRate
	}catch(e){
		throw new Error(`Unable to convert from ${from} to ${to}`);
	}
}

const getCountriesForCurrency = async (currency) => {
	try{
		const response = await axios.get(`https://restcountries.eu/rest/v2/currency/${currency}`)
		return response.data.map((country)=> country.name)
	}catch(e){
		throw new Error(`Unable to get countries that use ${currency}`)
	}
}

const convertCurrency = async (from, to, amount)=>{
	const rate = await getExchangeRate(from, to)
	const converted = (amount * rate).toFixed(2)
	const countries = await getCountriesForCurrency(to)
	return `${from} ${amount} = ${to} ${converted}. ${to} can be used in ${countries}`
}

convertCurrency('AED','QQQ',100).then((res)=>{
	console.log(res)
}).catch((e)=>console.log(e.message))