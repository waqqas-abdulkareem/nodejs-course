const express = require("express")

let app = express()

app.get('/', (req,res)=>{
	res.status(200)
	res.send("Hello World")
})

app.get('/users',(req,res)=>{
	res.send([{
		name: 'Harry Potter',
		age: 16
	},{
		name: 'Albus Dumbledore',
		age: 1000
	},{
		name: 'Dolores Umbridge',
		age: 50
	}])
})

app.listen(3000,()=>{
	console.log('server is up')
})

module.exports.app = app