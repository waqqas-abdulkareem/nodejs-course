const {MongoClient, ObjectID} = require('mongodb')

let obj = new ObjectID()
console.log(obj)

MongoClient.connect('mongodb://localhost:27017/todos',(err,client)=>{
	if (err) {
		return console.log(`Unable to connect to mongodb: ${err}`)
	}
	console.log('Connected to mongodb')

	const db = client.db('todos')

	// db.collection('Todos').deleteMany({text: 'Eat Lunch'}).then((res)=>{
	// 	console.log(res)
	// })

	// db.collection('Todos').deleteMany({text: 'Eat Dinner'}).then((res)=>{
	// 	console.log(res)
	// })	

	// db.collection('Todos').findOneAndDelete({completed: false}).then((res)=>{
	// 	console.log(res)
	// })

	// db.collection('Users').deleteMany({name: 'Harry Potter2'}).then((res)=>{
	// 	console.log(res)
	// })

	db.collection('Users').findOneAndDelete({_id: ObjectID("5b1ff7ab984f07335cbf8a46")}).then((res)=>{
		console.log(res)
	})

	//client.close()
})