const request = require("supertest")
let expect = require("expect")
let app = require("./server.js").app

describe('server',()=>{
	describe('# GET /',()=>{

		it('should return hello world response',(done)=>{
			request(app)
				.get('/')
				.expect(200)
				.expect('Hello World')
				.end(done)
		})

	})

	describe('# GET /users',()=>{

		it('should return an array of users', (done)=>{
			request(app)
				.get('/users')
				.expect(200)
				.expect((res)=>{
					expect(res.body).toInclude({
						name: 'Harry Potter',
						age: 16
					})
				})
				.end(done)
		})

	})
})