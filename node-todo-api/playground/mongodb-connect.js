const {MongoClient, ObjectID} = require('mongodb')

let obj = new ObjectID()
console.log(obj)

MongoClient.connect('mongodb://localhost:27017/todos',(err,client)=>{
	if (err) {
		return console.log(`Unable to connect to mongodb: ${err}`)
	}
	console.log('Connected to mongodb')

	const db = client.db('todos')

	// db.collection('Todos').insertOne({
	// 	text: 'Go out for dinner',
	// 	completed: false
	// },(err,res)=>{
	// 	if(err){
	// 		return console.log(`Failed to insert todo: ${err}`)
	// 	}
	// 	return console.log(JSON.stringify(res,undefined,2))
	// })

	db.collection('Users').insertOne({
		name: 'Harry Potter2',
		age: 28,
		location: 'Hogwarts'
	},(err,res)=>{
		if (err) {
			return console.log(`Failed to insert user: ${err}`)
		}
		return console.log(JSON.stringify(res.ops[0]._id.getTimestamp(),undefined,2))
	})

	client.close()
})