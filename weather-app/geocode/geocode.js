const axios = require("axios");

let geocodeAddress = (address) => {
	
	let encodedAddress = encodeURIComponent(address);
	let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`
	
	return axios.get(url).then((response)=>{
		let body = response.data;
		if (body.status === "OK") {
			return {
				formattedAddress: body.results[0].formatted_address,
				latitude: body.results[0].geometry.location.lat,
				longitude: body.results[0].geometry.location.lng
			};
		}else{
			throw new Error(body.error_message);
		}
	});
}

module.exports = {
	geocodeAddress
}