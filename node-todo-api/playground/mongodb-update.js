const {MongoClient, ObjectID} = require('mongodb')

let obj = new ObjectID()
console.log(obj)

MongoClient.connect('mongodb://localhost:27017/todos',(err,client)=>{
	if (err) {
		return console.log(`Unable to connect to mongodb: ${err}`)
	}
	console.log('Connected to mongodb')

	const db = client.db('todos')

	// db.collection('Todos').findOneAndUpdate({_id: ObjectID("5b20136b0109bd30c8e2ae2a")},{$set:{completed: false}},{returnOriginal: false}).then((res)=>{
	// 	console.log(res)
	// })

	db.collection('Users').findOneAndUpdate({_id: ObjectID("5b1ff6d3791cfc334bf3458a")},{$set: {name: 'Hermione Granger'}},{returnOriginal: false}).then((res)=>{
		console.log(res)
	})

	client.close()
})