const axios = require("axios");
const darkSkyAPIKey = "627efd6e58daf916bd042db7dc373693"

let getWeather = (lat, lng) => {
	let url = `https://api.darksky.net/forecast/${darkSkyAPIKey}/${lat},${lng}?units=si`

	return axios.get(url).then((response)=>{

		let body = response.data;
		if(response.status === 200){
			return {
				temperature: body.currently.temperature,
				apparentTemperature: body.currently.apparentTemperature
			};
		}else{
			throw new Error("Invalid request");
		}
	})
}

module.exports = {
	getWeather
}