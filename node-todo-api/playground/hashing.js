const {SHA256} = require('crypto-js')

let message = 'A String'
let hash = SHA256(message).toString()

console.log(`message: ${message}\nhash: ${hash}`)