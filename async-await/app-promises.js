const users = [{
	id: 1,
	name: 'Harry',
	schoolId: 101
},{
	id: 2,
	name: 'Victor',
	schoolId: 202
},{
	id: 3,
	name: 'Ron',
	schoolId: 909
}]

const grades = [{
	id: 1,
	schoolId: 101,
	grade: 86
},{
	id: 1,
	schoolId: 202,
	grade: 75
},{
	id: 3,
	schoolId: 101,
	grade: 10
}]

const getUser = (id)=>{
	return new Promise((resolve,reject)=>{
		const user = users.find((user)=> user.id === id )

		if (user) {
			resolve(user)
		}else{
			reject(`Unable to find user with id ${id}`)
		}
	})
}

const getGrades = (schoolId) => {
	return new Promise((resolve,reject)=>{
		resolve(grades.filter((grade) => grade.schoolId === schoolId ))
	})
}

const getStatus = (userId) =>{
	return new Promise((resolve, reject)=>{
		let user;
		getUser(userId)
		.then((_user)=> {
			user = _user;
			return getGrades(user.schoolId)
		}).then((grades)=>{
			if (grades.length === 0) {
				return reject(`User ${user.name} has no grades`)
			}
			const total = grades.map((it)=>it.grade).reduce((a,b) => a+b)
			const average = total / grades.length
			resolve(`${user.name} has an average of ${average}%`)
		
		}).catch((e)=>reject(e))
	})
}

const getStatusAlt = async (userId) => {
	const user = await getUser(userId)
	const grades = await getGrades(user.schoolId)
	if (grades.length === 0) {
		throw new Error(`User ${user.name} has no grades`)
	}
	const total = grades.map((it)=>it.grade).reduce((a,b) => a+b)
	const average = total / grades.length
	return `${user.name} has an average of ${average}%`
}

// getUser(1)
// .then((user)=>console.log(user))
// .catch((e)=>console.log(e))

// getGrades(101)
// .then((grades)=>console.log(grades))
// .catch((e)=>console.log(e))

// getStatus(2)
// .then((status)=>console.log(status))
// .catch((e)=>console.log(e))

// getStatusAlt(3)
// .then((name)=>console.log(name))
// .catch((e)=>console.log(e))


const add = async (a,b) =>  a + b + x

const doWork = async ()=>{
	try{
		const result = await add(1,2)
		return result
	}catch(e){
		return 0
	}
}

doWork()
.then((r)=>console.log(r))
.catch((e)=>console.log('Something went wrong'))


