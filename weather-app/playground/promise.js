let asyncAdd = (a,b) => {
	return new Promise((resolve, reject)=>{
		setTimeout(()=>{
			if (typeof a === "number" && typeof b === "number") {
				resolve(a + b);
			}else{
				reject(`Both arguments must be a number but a is ${typeof a} and b is ${typeof b}).`);
			}
		},2500);
	});
}

asyncAdd(1,"1").then((sum)=>{
	console.log(`Sum is ${sum}`);
	return asyncAdd(sum,50)
}).then((sum2)=>{
	console.log(`Sum2 is ${sum2}`);
}).catch((err)=>{
	console.log(err);
})

// let somePromise = new Promise((resolve, reject)=>{
// 	setTimeout(() =>{
// 		reject("Failure")
// 	},2500);
// });

// somePromise.then((message)=>{
// 	console.log(message);
// },(error)=>{
// 	console.log(error);
// })