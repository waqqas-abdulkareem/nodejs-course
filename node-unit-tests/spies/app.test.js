const expect = require('expect')
const rewire = require('rewire')

let app = rewire('./app.js')

describe('App',()=>{
	let db = {
		saveUser: expect.createSpy()
	}
	app.__set__('db',db)
	it('should call save user',()=>{
		let email = 'a@b.com'
		let password = '123'

		app.handleSignup({email,password})

		expect(db.saveUser).toHaveBeenCalled({email,password})
	})

})