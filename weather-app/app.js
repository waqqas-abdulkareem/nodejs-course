const yargs = require("yargs");
const geocode = require("./geocode/geocode.js")
const weather = require("./weather/weather.js")

const argv = yargs
	.options({
		a: {
			describe: "Address",
			demand: true,
			alias: "address",
			string: true
		}
	})
	.help()
	.alias("h")
	.argv;

geocode.geocodeAddress(argv.address).then((address)=>{
	return weather.getWeather(address.latitude,address.longitude)
}).then((temps)=>{
	console.log(`Current Temp: ${temps.temperature}. Feels like ${temps.apparentTemperature}`)
}).catch((error)=>{
	console.log(`error: ${error}`);
});