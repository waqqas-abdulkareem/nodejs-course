const expect = require("expect")
const utils = require("./utils.js")

describe('utils',()=>{

	it('should add two numbers', ()=>{
		// var got = utils.add(2,2)
		// let expected = 4
		// if (got !== expected) {
		// 	throw new Error(`Expected ${expected}. Got ${got}`)
		// }
		expect(utils.add(2,2)).toBe(4)
	})

	it('should square numbers', ()=>{
		expect(utils.square(5)).toBe(25).toBeA('number')
	})

	it('should verify first and last name are set', ()=>{
		let user = {}
		utils.setName(user,"Lord Voldemort")

		expect(user)
		.toInclude({
			firstName: "Lord",
			lastName: "Voldemort"}
		)
	})

	describe('#async',()=>{

		it('should add two numbers asynchronously', (done)=>{
			utils.asyncAdd(4,3,(sum)=>{
				expect(sum).toBe(7).toBeA('number')
				done()
			})
		})

		it('should square two numbers asynchronously', (done)=>{
			utils.asyncSquare(5,(square)=>{
				expect(square).toBe(25).toBeA('number')
				done()
			})
		})

	})
})