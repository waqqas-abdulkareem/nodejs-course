const {ObjectID} = require('mongodb')
const {mongoose} = require('./../server/db/mongoose')
const {Todo} = require('./../server/models/todo')
const id = '5b236683e22eab439af2031c'

Todo.find({_id: id}).then((doc)=>{
	console.log('Todos:',doc)
})

Todo.findOne({_id: id}).then((doc)=>{
	console.log('Todo:',doc)
})

Todo.findById(id).then((doc)=>{
	console.log(`Todo with id '${id}': ${doc}`)
})

const invalidId = id+11
// Todo.findById(invalidId).then((doc)=>{
// 	console.log(doc)
// }).catch((e) => console.log('ERROR'))

if (!ObjectID.isValid(invalidId)) {
	console.log('id not valid')
}