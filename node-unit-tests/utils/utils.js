module.exports.add = (a,b) => a + b
module.exports.square = (a) => a * a
module.exports.setName = (user,fullName) =>{
	let names = fullName.split(" ")
	user.firstName = names[0]
	user.lastName = names[1]
}
let that = this

module.exports.asyncAdd = (a,b,callback) =>{
	setTimeout(()=>{
		callback(this.add(a,b))
	},1000)
}

module.exports.asyncSquare = (a,callback) => {
	setTimeout(()=>{
		callback(this.square(a))
	},1000)
}