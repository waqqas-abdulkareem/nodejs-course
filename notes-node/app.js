const fs = require("fs");
const _ = require("lodash")
const yargs = require("yargs");

const notes = require("./notes.js");

const titleOptions = {
	describe: "Title of the note",
	demand: true,
	alias: "t"
};

const bodyOptions = {
	describe: "Content of the note",
	demand: true,
	alias: "b"
};

const argv = yargs
	.command("add","Adds a note", {
		title: titleOptions,
		body: bodyOptions
	})
	.command("list","List all notes")
	.command("read","Read a note", {
		title: titleOptions
	})
	.command("remove","Remove a note", {
		title: titleOptions
	})
	.help()
	.argv;
var command = argv._[0];

if (command === 'add') {
	let note = notes.addNote(argv.title, argv.body);
	if (note == null) {
		console.log(`A note with '${argv.title}' already exists`);
	}else{
		console.log(`Note with title '${note.title}' created`);
	}
}else if (command === 'list') {
	let allNotes = notes.getAll();
	console.log(`Printing ${allNotes.length} notes:\n`);
	allNotes.forEach((note) => console.log(note.title+"\n") );
}else if (command === 'read') {
	let note = notes.getNote(argv.title);
	if (note == null) {
		console.log("Note not found");
	}else{
		console.log(note.body);
	}
}else if (command === 'remove') {
	console.log(notes.removeNote(argv.title)? "Removed" : "Not Found")
}else{
	console.log("Unknown command");
} 