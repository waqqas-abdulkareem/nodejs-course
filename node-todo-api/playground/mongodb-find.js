const {MongoClient,ObjectID} = require('mongodb')

MongoClient.connect('mongodb://localhost:27017/todos',(err,client)=>{
	if (err) {
		return console.log(`Unable to connect to mongodb: ${err}`)
	}
	console.log('Connected to mongodb')

	const db = client.db('todos')

	db.collection('Todos').find({_id: ObjectID("5b20136b0109bd30c8e2ae2a")}).toArray().then((docs)=>{
		console.log(`Results: ${JSON.stringify(docs,undefined,2)}`)
	},(err)=>{
		console.log(`Error: ${err}`)
	})

	db.collection('Todos').find().count().then((count)=>{
		console.log(`${count} Records`)
	},(err)=>{
		console.log(`Error: ${err}`)	
	})

	db.collection('Users').find({name: 'Harry Potter2'}).toArray().then((docs)=>{
		console.log(`Users: ${JSON.stringify(docs,undefined,2)}`)
	},(err)=>{
		console.log(`Error: ${err}`)
	})

})